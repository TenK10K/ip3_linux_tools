#!/bin/bash
#echo copying intel firmware
#tar -xvJf ./intel/data.tar.xz 
#cp -r ./lib/firmware/intel/sof/* /lib/firmware/intel/sof/

echo install tools & intel firmware
dpkg -i ./tools/*.deb

echo copy topology
cp ./tplg/dmic/* /lib/firmware/intel/sof-tplg/

#echo get dsdt
#cat /sys/firmware/acpi/tables/DSDT > dsdt.dat
#iasl -d dsdt.dat

#echo get gpio
#cat /sys/kernel/debug/gpio>gpio.txt

echo allow selecting kernel from grub
#cp grub /etc/default/grub

#echo enableing dynamic debug
#echo 'options snd_soc_es8316 dyndbg' >> /etc/modprobe.d/blacklist.conf
#echo 'options snd_soc_hdac_hdmi dyndbg' >> /etc/modprob.d/blacklist.conf
#echo 'options snd_hda_codec_hdmi dyndbg' >> /etc/modprob.d/blacklist.conf
#echo 'options snd_sof_intel_hda_common dyndbg' >> /etc/modprob.d/blacklist.conf

echo setting up hdmi device
#echo 'load-module module-alsa-source device=hw:0,1 sink_name=DMIC' >> /etc/pulse/default.pa
echo 'load-module module-alsa-sink device=hw:0,5 sink_name=HDMI sink_properties=device.description=HDMI' >> /etc/pulse/default.pa
#echo '(sleep 12; pulseaudio -k) &' >> ~/.profile
echo '(sleep 16; pacmd set-default-sink 0) &' >> ~/.profile
echo '(sleep 5; pacmd set-default-sink 2) &' >> ~/.profile
echo 'amixer sset IEC958 on' >> ~/.profile

