#!/bin/bash
#echo copying intel firmware
#tar -xvJf ./intel/data.tar.xz 
#cp -r ./lib/firmware/intel/sof/* /lib/firmware/intel/sof/

#apt-get update
#apt-get install git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison

echo install intel firmware
#sudo dpkg -i ./tools/*.deb
sudo dpkg -i --force-overwrite ./tools/firm*.deb
echo copy topology
cp ./tplg/amic/* /lib/firmware/intel/sof-tplg/

#echo get dsdt
#cat /sys/firmware/acpi/tables/DSDT > dsdt.dat
#iasl -d dsdt.dat

#echo get gpio
#cat /sys/kernel/debug/gpio>gpio.txt

echo allow selecting kernel from grub
cp grub /etc/default/grub

#echo 'options snd_soc_es8316 dyndbg' >> /etc/modprobe.d/blacklist.conf
#echo 'options snd_soc_hdac_hdmi dyndbg' >> /etc/modprob.d/blacklist.conf
#echo 'options snd_hda_codec_hdmi dyndbg' >> /etc/modprob.d/blacklist.conf
#echo 'options snd_sof_intel_hda_common dyndbg' >> /etc/modprob.d/blacklist.conf

echo setting up hdmi device
sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa
#sed -i '28i\load-module module-alsa-sink device=hw:0,5 sink_name=HDMI sink_properties=device.description=HDMI' /etc/pulse/default.pa
#echo 'load-module module-alsa-sink device=hw:0,5 sink_name=HDMI sink_properties=device.description=HDMI' >> /etc/pulse/default.pa
#echo '(sleep 12; pulseaudio -k) &' >> ~/.profile
#echo '(sleep 6; pacmd set-default-sink 2) &' >> ~/.profile
#echo '(sleep 5; pacmd set-default-sink 0) &' >> ~/.profile
#echo 'amixer sset IEC958 on' >> ~/.profile

#echo setting up alsamixer
#amixer cset numid=3 999,999
