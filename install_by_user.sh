#!/bin/bash

project=`sudo dmidecode -t 0 | grep 'Version:'`

if [[ $project =~ "Version: TN29" ]] ; then
echo "========TN29========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo cp ./ip3_tn29/HiFi.conf /usr/share/alsa/ucm2/sof-essx8336/HiFi.conf

sudo dpkg -i ./kernel/deb_linux-5.18.1_20220531_2100_es8336/*.deb

sudo dpkg -i ./ip3_tn29/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

elif [[ $project =~ "Version: GN10" ]] ; then
echo "========GN10========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo dpkg -i ./kernel/deb_linux-5.18.1_20220531_2100_es8336/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

elif [[ $project =~ "Version: JN23" ]] ; then
echo "========JN23========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo cp ./ip3_jn23/sof-jsl_1.9.2.ri /lib/firmware/intel/sof/sof-jsl.ri
sudo cp ./ip3_jn23/sof-glk-es8336-ssp1-mclkssp0.tplg /lib/firmware/intel/sof-tplg/sof-jsl-es8336-ssp1.tplg

sudo dpkg -i ./ip3_jn23/deb_linux-5.18.1_20220601_1852_es8336_jn23/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

elif [[ $project =~ "Version: TB20" ]] ; then
echo "========TB20========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo cp /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic2ch-ssp0.tplg /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic1ch-ssp0.tplg
sudo cp ./ip3_tb20/HiFi.conf /usr/share/alsa/ucm2/sof-essx8336/HiFi.conf

sudo dpkg -i ./ip3_tb20/deb_linux-5.18_20220525_2122_es8316_tb20/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

#sudo sed -i 's/enabled.*=.*/enabled=1/g' /etc/default/apport

elif [[ $project =~ "Version: CN23" ]] ; then
echo "========CN23========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo dpkg -i ./kernel/deb_linux-5.18.1_20220531_2100_es8336/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

elif [[ $project =~ "Version: TGUT1" ]] ; then
##ALC897
echo "========TGUT1========"

sudo dpkg -i ./ip3_tgut1_alc897/deb_linux-5.18.1_20220531_2024_tgut1_alc897/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

else
echo "========IP3========"

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo dpkg -i ./kernel/deb_linux-5.18.1_20220531_2100_es8336/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

fi

#sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
#sudo sed -i 's/GRUB_DEFAULT.*=.*/GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 5.18.0-rc2-tgl"/g' /etc/default/grub
#sudo sh -c 'echo "GRUB_GFXPAYLOAD_LINUX=keep" >> /etc/default/grub'

sudo update-grub



#sudo reboot
#sudo systemctl unmask sleep.target suspend.target hibernate.target hybrid-sleep.target
